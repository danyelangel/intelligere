var functions = require('firebase-functions')
var admin = require('firebase-admin')

exports.function = functions
  .database
  .ref('{env}/users/{uid}/DELETE')
  .onCreate((change, ctx) => {
    const uid = ctx.params.uid
    const env = ctx.params.env
    console.log(uid, env)
    return admin.auth().deleteUser(uid).catch(() => true)
      .then(() => admin.database().ref(`${env}/users/${uid}`).remove())
  })