const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
exports.function = functions
  .https
  .onRequest((req, res) => {
    cors(req, res, () => {
      admin.auth().getUserByEmail(req.query.email).then(({uid}) => {
        admin.database().ref('production/users').child(uid).child('institutionId').once('value').then(snap => snap.val())
          .then(institutionId => res.send(institutionId ? '' : uid))
      }).catch((err) => {
        console.log(err)
        res.status(500).end()
      })
    })
  })