var functions = require('firebase-functions');
var admin = require("firebase-admin");
(function (exports, functions, admin) {
  'use strict';
  exports.function = functions
    .auth
    .user()
    .onCreate(auth => {
      let uid = auth.uid,
          email = auth.email,
          displayName = auth.displayName;
      return Promise
        .all([
          admin
            .database()
            .ref('staging/users')
            .child(uid)
            .update({
              email,
              displayName
            }),
          admin
            .database()
            .ref('production/users')
            .child(uid)
            .update({
              email,
              displayName
            })
        ]);
    });
}(exports, functions, admin));