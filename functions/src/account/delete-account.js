var functions = require('firebase-functions');
var admin = require("firebase-admin");
(function (exports, functions, admin) {
  'use strict';
  exports.function = functions
    .auth
    .user()
    .onDelete(event => {
      let uid = event.data.uid;
      return Promise
        .all([
          admin
            .database()
            .ref('staging/users')
            .child(uid)
            .set(null),
          admin
            .database()
            .ref('production/users')
            .child(uid)
            .set(null)
        ]);
    });
}(exports, functions, admin));