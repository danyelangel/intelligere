const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
const axios = require('axios')
const template = require('../email').default
exports.function = functions
  .https
  .onRequest((req, res) => {
    cors(req, res, () => {
      const {displayName, email, password, goToGroups} = req.body
      admin.auth().createUser({
        email,
        password,
        displayName
      })
        .then(({uid}) => admin.database()
          .ref(`production/users/${uid}`).set({
            displayName,
            email
          })
          .then(() => admin.auth().generatePasswordResetLink(email, {
            url: goToGroups ? 'https://groups.intelligere.co' : 'https://app.intelligere.co'
          }))
          .then(url => sendEmail({ displayName, email, url }))
          .then(() => uid))
        .then((uid) => res.send(uid))
        .catch(err => {
          console.log(err)
          res.status(500).end()
        })
    })
  })
  function sendEmail ({ displayName, email, url }) {
    const html = template({
      title: 'Tu cuenta ha sido creada.',
      body: `
            Hola ${displayName}!
        <br>
        <br>Tu cuenta para usar Intelligere ha sido exitosamente creada.
        <br>
        <br>Para continuar, crea tu contraseña acá: ${url}.
        <br>
        `,
      url,
      cta: { text: 'Crea tu contraseña', padding: '0.5rem' },
      pda: `
        <br>Saludos,
        <br>Tu equipo de Inteligere`,
      footer: 'Intelligere &copy; 2019'
    })
    return axios({
      method: 'post',
      url: 'https://us-central1-project-867618968114337534.cloudfunctions.net/sendArbitraryEmail',
      data: {
        html
      },
      params: {
        to: email,
        from: 'accounts@intelligere.co',
        fromName: 'Intelligere Lectura Rápida',
        subject: 'Bienvenido a intelligere',
        token: 'MN55LHSAWi0knBa5vVgkA4HJuhT41vEA'
      }
    })
  }