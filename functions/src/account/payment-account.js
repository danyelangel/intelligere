const functions = require('firebase-functions')
const admin = require('firebase-admin')
const _ = require('lodash')
const cors = require('cors')({ origin: true })

exports.function = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.method === 'POST') return processRequest()
    function processRequest () {
      const {
        email_buyer: email,
        response_message_pol: responseMessage,
      } = req.body
      console.log(req.body, req.query)
      console.log(email)
      return admin.auth().getUserByEmail(email || req.query.email).then(({uid}) => {
        console.log(responseMessage, uid)
        return responseMessage === 'APPROVED' && admin.database().ref('production/users').child(uid).child('isMember').set(true)
      }).then(() => res.end()).catch((err) => console.log(err) || res.end())
    }
  })
})