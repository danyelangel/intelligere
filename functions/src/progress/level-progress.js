var functions = require('firebase-functions');
var _ = require('underscore');
(function (exports, functions) {
  'use strict';
  function getExcersises(levelId, rootRef) {
    return rootRef
      .child('levels')
      .child(levelId)
      .child('excersises')
      .once('value')
      .then(snap => {
        return snap.val();
      });
  }
  function isFinished(progress, totalExcersises) {
    let completed = 0;
    _(progress).each(itemProgress => {
      if (itemProgress >= 100) {
        completed += 1;
      }
    });
    return completed >= totalExcersises
      ? true
      : null;
  }
  function checkLevel(progress, levelId, levelRef, rootRef) {
    return getExcersises(levelId, rootRef)
      .then((totalExcersises = 0) => {
        return levelRef
          .child('isFinished')
          .set(isFinished(progress, totalExcersises));
      });
  }
  exports.function = functions
    .database
    .ref('{env}/students/{userId}/progress/{levelId}')
    .onWrite(({ after }, { params }) => {
      let rootRef = after.ref.root.child(params.env),
          levelId = params.levelId,
          levelRef = after.ref,
          progress = after.val();
      return progress.isFinished || levelId === 'global'
        ? Promise.resolve()
        : checkLevel(progress, levelId, levelRef, rootRef);
    });
}(exports, functions));