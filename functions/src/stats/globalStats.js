const functions = require('firebase-functions')
const admin = require('firebase-admin')
const _ = require('lodash')
const cors = require('cors')({
  origin: true
})
const flow = require('lodash/fp/flow')
const map = require('lodash/fp/map')
const filter = require('lodash/fp/filter')
const flatten = require('lodash/fp/flatten')
const invoke = require('lodash/fp/invoke')
const toPairs = require('lodash/fp/toPairs')
const groupBy = require('lodash/fp/groupBy')
const mapValues = require('lodash/fp/mapValues')
const compact = require('lodash/fp/compact')
const keys = require('lodash/fp/keys')
const toArray = require('lodash/fp/toArray')
const min = require('lodash/fp/min')
const max = require('lodash/fp/max')
const sum = require('lodash/fp/sum')
const mean = require('lodash/fp/mean')
const size = require('lodash/fp/size')
const takeRight = require('lodash/fp/takeRight')
const take = require('lodash/fp/take')
const sortBy = require('lodash/fp/sortBy')
const fromPairs = require('lodash/fp/fromPairs')
function getAllScores () {
  return admin.database().ref('production')
    .child('students')
    .once('value')
    .then(invoke('val'))
    .then(flow(
      toPairs,
      map(([userId, student]) => _.map(student.scores, score => Object.assign({}, score, { userId }))),
      compact,
      map(toArray),
      flatten
    ))
}
function getGroupScores ({ groupId, institutionId }) {
  return admin.database().ref('production/institutions').child(institutionId).child('groups').child(groupId).child('members').once('value')
    .then(invoke('val'))
    .then(keys)
    .then(keys => admin.database().ref('production')
      .child('students')
      .once('value')
      .then(invoke('val'))
      .then(flow(
        toPairs,
        filter(([userId]) => _.includes(keys, userId)),
        map(([userId, student]) => _.map(student.scores, score => Object.assign({}, score, { userId }))),
        compact,
        map(toArray),
        flatten
      )))
}
function getStudentScores (studentId) {
  return admin.database().ref('production')
    .child('students')
    .child(studentId)
    .child('scores')
    .once('value')
    .then(invoke('val'))
    .then(flow(
      toArray,
      compact
    ))
}
function getLessonDefinitions () {
  return admin.database().ref('production')
    .child('structure/lessons')
    .once('value')
    .then(invoke('val'))
}
function processData ([scores, definitions]) {
  const userScores = _.groupBy(scores, 'userId')
  const category = data => flow(
    map(i => [i, flow(
      map(i),
      compact,
      getAccumulators
    )(data)]),
    fromPairs
  )([
    'percentage',
    'milliseconds',
    'wpm',
    'comprehension',
    'number'
  ])
  return {
    summary: {
      startDate: _(scores).map('timestamp').min(),
      lastDate: _(scores).map('timestamp').max(),
      totalDuration: _(scores).map('duration').sum(),
      avgProgress: _(category(scores)).map('progress').compact().mean()
    },
    users: size(userScores),
    sortedUsers: _.fromPairs(_.map(userScores, (user, userId) => [userId, category(user)])),
    lessons: _(scores)
      .groupBy('lessonId')
      .mapValues((list, lessonId) => getAccumulators(_.map(list, getMeaninfulValue), definitions[lessonId]))
      .value(),
    category: category(scores)
  }
}
function getAccumulators (data, definition) {
  const isRichAccummulator = _.isArray(_.head(data))
  const isWpm = isRichAccummulator && _.isObject(_.head(data)[2])
  const list = isRichAccummulator ? _.map(data, 0) : data
  const MAX_SAMPLE_SIZE = 5
  const SAMPLE_SIZE = size(list) > MAX_SAMPLE_SIZE * 2 ? MAX_SAMPLE_SIZE : Math.floor(size(list) / 2)
  const topAvg = flow(sortBy(_.identity), takeRight(SAMPLE_SIZE), mean)(list)
  const lowAvg = flow(sortBy(_.identity), take(SAMPLE_SIZE), mean)(list)
  const startAvg = flow(take(SAMPLE_SIZE), mean)(list)
  const currentAvg = flow(takeRight(SAMPLE_SIZE), mean)(list)
  const { optimalScore, unit } = definition || {}
  const meanVal = mean(list)
  const effectiveness = unit === 'milliseconds'
    ? Math.floor(100 * optimalScore / meanVal)
    : Math.floor(100 * meanVal / optimalScore)
  return {
    list,
    min: min(list),
    max: max(list),
    mean: meanVal,
    topAvg,
    lowAvg,
    startAvg,
    currentAvg,
    progress: lowAvg / topAvg,
    size: size(list),
    duration: isRichAccummulator && sum(_.map(data, 1)),
    wpm: isWpm && {
      wpm: getAccumulators(_.map(data, '2.wpm'), definition),
      comprehension: getAccumulators(_.map(data, '2.comprehension'), definition)
    },
    optimalScore,
    effectiveness,
    color: getColor(effectiveness),
    label: getLabel(effectiveness),
    unit
  }
}
function getColor (effectiveness) {
  let color = '#f44336';
  if (effectiveness > 100) {
    color = '#00BCD4';
  } else if (effectiveness > 75) {
    color = '#8BC34A';
  } else if (effectiveness > 60) {
    color = '#CDDC39';
  } else if (effectiveness > 30) {
    color = '#FF9800';
  } else if (effectiveness > 10) {
    color = '#FF5722';
  }
  return color;
}
function getLabel (effectiveness) {
  let label = 'Nivel muy bajo';
  if (effectiveness > 100) {
    label = 'Nivel excelente';
  } else if (effectiveness > 75) {
    label = 'Nivel bueno';
  } else if (effectiveness > 60) {
    label = 'Nivel aceptable';
  } else if (effectiveness > 30) {
    label = 'Nivel medio';
  } else if (effectiveness > 10) {
    label = 'Nivel bajo';
  }
  return label;
}
function getMeaninfulValue ({ milliseconds, percentage, wpm, number, comprehension, duration }, definition) {
  return [milliseconds || percentage || number || wpm && wpm * comprehension || 0, duration, wpm ? { wpm, comprehension } : null]
}
exports.function = functions
  .https
  .onRequest((req, res) => {
    cors(req, res, () => {
      const { studentId, groupId, institutionId } = req.query
      let scoresPromise
      if (studentId) {
        scoresPromise = getStudentScores(studentId)
      } else if (institutionId) {
        scoresPromise = getGroupScores({ groupId, institutionId })
      } else {
        scoresPromise = getAllScores()
      }
      return Promise
        .all([
          scoresPromise,
          getLessonDefinitions()
        ])
        .then(processData)
        .then(data => res.json(data))
    })
  })