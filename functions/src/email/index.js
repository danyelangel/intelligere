const template = require('./template').default
exports.default = function getTemplate (params) {
  //- Template
  const { bannerImg, logoImg, background: backgroundColor = 'rgb(239, 210, 141)', text, surface, cta } = params
  const { font = `'Helvetica', sans-serif`, titleColor = 'rgb(0, 71, 119)', bodyColor, align: textAlign } = text || {}
  const { shadow: surfaceShadow = '0 6px 6px -3px rgba(0,0,0,.2)', background: surfaceColor = '#fff', margin: surfaceMargin, padding: surfacePadding, borderRadius: borderRadius } = surface || {}
  const { text: ctaText, padding: ctaPadding, shadow: ctaShadow, background: ctaBackground = '#a30000', borderRadius: ctaBorderRadius, color: ctaColor = '#fff' } = cta || {}
  //- Data
  const { title, body, url: ctaUrl, pda, footer } = params
  return template({
    font,
    borderRadius,
    bannerImg,
    logoImg,
    backgroundColor,
    titleColor,
    bodyColor,
    textAlign,
    surfaceShadow,
    surfaceColor,
    surfaceMargin,
    surfacePadding,
    ctaText,
    ctaPadding,
    ctaShadow,
    ctaBackground,
    ctaBorderRadius,
    ctaColor
  })({
    title,
    body,
    url: ctaUrl,
    pda,
    footer
  })
}