const _ = require('lodash')
const attrs = att => _.map(_.mapValues(att || {}, item => _.isObject(item) ? _.map(item, (i, k) => `${k}:${i}`).join(';') : item), (data, id) => ` ${id}="${data}"`).join('')
const h = (el = 'div', att, inner = '') => `<${el}${attrs(att)}>${_.compact(inner).join('')}</${el}>`
// Head
const head = () => h('head', null, [
  h('meta', { charset: 'ISO-8859-1' }),
  h('meta', { name: 'viewport', content: 'width=device-width, initial-scale=1.0' })
])
// Logo
const logo = ({ textAlign, logoImg }) => h('tr', null, [
  textAlign !== 'left' && h('td', { width: '38%' }),
  textAlign === 'right' && h('td', { width: '38%' }),
  h('td', { width: '24%' }, [
    logoImg && h('img', { src: logoImg, style: { width: '100%' } })
  ]),
  textAlign === 'left' && h('td', { width: '38%' }),
  textAlign !== 'right' && h('td', { width: '38%' }),
])
// Banner
const banner = ({ bannerImg }) => h('tr', null, [
  h('td', { colspan: 3 }, [
    bannerImg && h('img', { src: bannerImg, style: { width: '100%' } })
  ])
])
// Content
const content = ({ surfacePadding, titleColor, bodyColor, textAlign, title, body }) => h('tr', null, [
  h('td', { colspan: 3, style: { padding: `${surfacePadding} 0` } }, [
    title && h('h1', { style: { color: titleColor, 'text-align': textAlign } }, title),
    body && h('p', { style: { color: bodyColor, 'text-align': textAlign } }, body),
  ])
])
// Call to Action
const cta = ({ textAlign, ctaUrl, ctaBackground, ctaShadow, ctaBorderRadius, ctaPadding, ctaColor, ctaText }) => h('tr', null, [
  textAlign !== 'left' && h('td', { width: '30%' }),
  h('td', { width: '40%', style: { background: ctaBackground, 'text-align': 'center', 'box-shadow': ctaShadow, 'border-radius': ctaBorderRadius } }, [
    ctaUrl && h('a', { href: ctaUrl, target: '_blank', style: { display: 'block', width: `calc(100% - ${ctaPadding} - ${ctaPadding})`, padding: ctaPadding, color: ctaColor, 'text-decoration': 'none' } }, ctaText),
  ]),
  textAlign !== 'right' && h('td', { width: '30%' })
])
// Post data
const postdata = ({ pda, bodyColor, textAlign }) => h('td', { colspan: 3 }, [
  pda && h('p', { style: { color: bodyColor, 'text-align': textAlign, margin: '20px 0' } }, pda),
])
// Separator
const separator = ({ textAlign, bodyColor }) => h('tr', null, [
  textAlign !== 'left' && h('td'),
  h('td', { width: '20%', style: { background: bodyColor, height: '1px', margin: 0 } }),
  textAlign !== 'right' && h('td')
])
// Footer
const footerBlock = ({ footer, bodyColor, textAlign }) => h('td', { colspan: 3 }, [
  footer && h('p', { style: { color: bodyColor, 'text-align': textAlign, margin: '20px 0' } }, footer),
])
// Email
const email = ({ surfaceColor, surfacePadding, surfaceShadow, borderRadius, textAlign, logoImg, bannerImg, titleColor, bodyColor, title, body, ctaUrl, ctaBackground, ctaShadow, ctaBorderRadius, ctaPadding, ctaColor, ctaText, pda, footer }) => h('div', { style: { width: '100%', 'max-width': '600px', background: surfaceColor, padding: surfacePadding, 'box-shadow': surfaceShadow, 'border-radius': borderRadius } }, [
  h('table', { style: { width: '100%' } }, [
    logo({ textAlign, logoImg }),
    // BANNER
    banner({ bannerImg }),
    // TITLE + BODY
    content({ surfacePadding, titleColor, bodyColor, textAlign, title, body }),
  ]),
  h('table', { style: { width: '100%', margin: '10px 0' } }, [
    // CTA BUTTON
    cta({ textAlign, ctaUrl, ctaBackground, ctaShadow, ctaBorderRadius, ctaPadding, ctaColor, ctaText }),
    // POST DATA
    postdata({ pda, bodyColor, textAlign })
  ]),
  h('table', { style: { width: '100%' } }, [
    // SEPARATOR
    separator({ textAlign, bodyColor }),
    // FOOTER
    footerBlock({ footer, bodyColor, textAlign })
  ])
])
exports.default = ({ bannerImg, logoImg, backgroundColor, borderRadius, surfaceColor, surfaceShadow, surfaceMargin, surfacePadding, titleColor, bodyColor, font, ctaText, textAlign, ctaBackground, ctaColor, ctaPadding, ctaShadow, ctaBorderRadius }) => ({ url: ctaUrl, title, body, pda, footer }) => h('html', null, [
  head(),
  h('body', { style: { 'font-family': font, background: backgroundColor, margin: surfaceMargin } }, [
    email({ surfaceColor, surfacePadding, surfaceShadow, borderRadius, textAlign, logoImg, bannerImg, titleColor, bodyColor, title, body, ctaUrl, ctaBackground, ctaShadow, ctaBorderRadius, ctaPadding, ctaColor, ctaText, pda, footer })
  ])
])