const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
const axios = require('axios')
const template = require('../email').default
exports.function = functions
  .https
  .onRequest((req, res) => {
    cors(req, res, () => {
      const { user, metadata } = req.body
      const {displayName, email, password} = user
      const institutionId = admin.database().ref().push().key
      if (req.body) {
        admin.auth().createUser({
          email,
          password,
          displayName
        })
          .then(({uid}) => Promise.all([
            addUserToDatabase({ uid, displayName, email, institutionId }),
            createInstitution({ metadata, institutionId, uid })
          ]))
          .then(() => sendEmail({ email, password, displayName, institution: metadata }))
          .then(() => res.end())
          .catch(err => {
            console.log(err)
            res.status(500).end()
          })
      }
    })
  })
function addUserToDatabase ({ uid, displayName, email, institutionId }) {
  return admin.database()
    .ref(`production/users/${uid}`).set({
      displayName,
      email,
      institutionId
    })
}
function createInstitution ({ uid, metadata, institutionId }) {
  return admin.database()
    .ref(`production/institutions/${institutionId}`)
    .set({
      adminId: uid,
      metadata,
      timestamp: {
        '.sv': 'timestamp'
      }
    })
}
function sendEmail ({ displayName, email, password, institution: { displayName: institutionName } }) {
  const html = template({
    title: 'Tu institución ha sido creada.',
    body: `
          Hola ${displayName}!
      <br>
      <br>Tu cuenta grupal para ${institutionName} ha sido exitosamente creada.
      <br>Para ingresar ve a https://groups.intelligere.co e inicia sesión con tus credenciales.
      <br>Estos son los credenciales para ingresar:
      <br>
      <br><b>Email:</b>&nbsp;${email}
      <br><b>Contraseña:</b>&nbsp;${password}`,
    url: 'https://groups.intelligere.co',
    cta: { text: 'Ingresa aquí al portal', padding: '0.5rem' },
    pda: `
      <br>Saludos,
      <br>Tu equipo de Inteligere`,
    footer: 'Intelligere &copy; 2019'
  })
  return axios({
    method: 'post',
    url: 'https://us-central1-project-867618968114337534.cloudfunctions.net/sendArbitraryEmail',
    data: {
      html
    },
    params: {
      to: email,
      from: 'accounts@intelligere.co',
      fromName: 'Intelligere Lectura Rápida',
      subject: 'Bienvenido a Intelligere Institucional',
      token: 'MN55LHSAWi0knBa5vVgkA4HJuhT41vEA'
    }
  })
}