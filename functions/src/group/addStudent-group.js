var functions = require('firebase-functions')
var admin = require('firebase-admin')
const axios = require('axios')
const template = require('../email').default
exports.function = functions.database
.ref('{env}/institutions/{institutionId}/groups/{groupId}/members/{memberId}')
  .onCreate((s, {params: {groupId, institutionId, env}}) => {
    const {displayName, email, password} = s.val()
    const {ref} = s
    return admin.auth().createUser({email, password}).then(({uid}) => {
      return admin.database().ref(env)
        .child('users')
        .child(uid)
        .set({
          displayName,
          email,
          institutionId
        })
        .then(() => {
          return Promise.all([
            admin.database().ref(env)
              .child('institutions')
              .child(institutionId)
              .child('groups')
              .child(groupId)
              .child('members')
              .child(uid)
              .set({
                displayName,
                email
              }).catch(() => console.log('FAILED GROUP')),
            admin.database().ref(env)
              .child('institutions')
              .child(institutionId)
              .child('members')
              .child(uid)
              .set({
                displayName,
                email,
                groupId
              }).catch(() => console.log('FAILED MEMBER')),
            ref.set(null).catch(() => console.log('FAILED ADMINREF'))
          ])
        })
    }).then(() => {
      console.log('EMAIL SENDING')
    }).catch(console.log)
  })
