var functions = require('firebase-functions');
(function (exports, functions) {
  'use strict';
  exports.function = functions
    .database
    .ref('{env}/users/{userId}/groupId')
    .onWrite(({ after }, { params }) => {
      const rootRef = after.ref.root.child(params.env)
      const userId = params.userId
      const groupId = after.val()
      function setAsMember() {
        return rootRef
          .child('users')
          .child(userId)
          .child('isMember')
          .set(true)
      }
      function getMemberProfile() {
        return rootRef
          .child('users')
          .child(userId)
          .once('value')
          .then(snap => snap.val())
      }
      function addToGroup({displayName, email}) {
        return rootRef
          .child('groups')
          .child(groupId)
          .child('members')
          .child(userId)
          .update({
            displayName,
            email
          })
      }
      return Promise.all([
        setAsMember(),
        getMemberProfile().then(({displayName, email}) => addToGroup({displayName, email}))
      ])
    });
}(exports, functions));