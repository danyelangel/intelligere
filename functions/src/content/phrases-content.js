var functions = require('firebase-functions');
var htmlToText = require('html-to-text');
var _ = require('underscore');
(function (exports, functions, htmlToText, _) {
  'use strict';
  function savePhrases(phrases, articleId, rootRef) {
    return rootRef
      .child('content/phrases')
      .child(articleId)
      .set(phrases);
  }
  exports.function = functions
    .database
    .ref('{env}/content/articles/{articleId}/content/body')
    .onWrite(event => {
      let rootRef = event.data.adminRef.root.child(event.params.env),
          articleId = event.params.articleId,
          articleHtml = event.data.val(),
          article = htmlToText.fromString(articleHtml, {
            wordwrap: null
          }),
          phrases = _(article.replace(/(\r\n|\n|\r)/gm, ' ').split('. ')).map(phrase => {
            return {
              content: phrase.trim(),
              length: phrase.length
            };
          });
      return savePhrases(phrases, articleId, rootRef);
    });
}(exports, functions, htmlToText, _));