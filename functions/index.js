var functions = require('firebase-functions');
var admin = require("firebase-admin");
var _ = require('underscore');

admin.initializeApp(functions.config().firebase);

// Account
exports['accountCreate'] = require('./src/account/create-account.js').function;
exports['accountDelete'] = require('./src/account/delete-account.js').function;
exports['accountRequestDeletion'] = require('./src/account/requestDeletion-account.js').function;
exports['accountSearch'] = require('./src/account/search-account.js').function;
exports['accountRequest'] = require('./src/account/request-account.js').function;
exports['accountPayment'] = require('./src/account/payment-account.js').function;

// Institution
exports['createInstitution'] = require('./src/institution/createInstitution.js').function;

// Group
exports['groupJoin'] = require('./src/group/join-group.js').function;
exports['groupAddStudent'] = require('./src/group/addStudent-group.js').function;

// Content
exports['contentPhrases'] = require('./src/content/phrases-content.js').function;

// Progress
exports['progressLevel'] = require('./src/progress/level-progress.js').function;

// Progress
exports['globalStats'] = require('./src/stats/globalStats.js').function;
