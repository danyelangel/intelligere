# intelligere-landing

*Generated with [ng-poly](https://github.com/dustinspecker/generator-ng-poly/tree/v0.13.0) version 0.13.0*

## Setup
1. Install [Node.js](http://nodejs.org/)
 - This will also install npm.
2. Run `yarn` or `npm install` to install dependencies
3. Run `bower install` to install client-side dependencies
4. Run `yarn dev` to develop
5. Run `yarn deploy` to deploy to Firebase

## Gulp tasks
- Run `gulp build` to compile assets
- Run `gulp dev` to run the build task and setup the development environment
- Run `gulp unitTest` to run unit tests via Karma and to create code coverage reports
- Run `gulp webdriverUpdate` to download Selenium server standalone and Chrome driver for e2e testing
- Run `gulp e2eTest` to run e2e tests via Protractor
 - **A localhost must be running** - `gulp dev`