(function () {
  'use strict';
  class Controller {}
  angular
    .module('landing.about.slider', [])
    .component('landingAboutSlider', {
      controller: Controller,
      templateUrl: 'root/landing/_components/about/slider/slider-about-landing.comp.html'
    });
}());
