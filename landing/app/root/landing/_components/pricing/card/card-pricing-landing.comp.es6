(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('landing.pricing.card', [])
    .component('landingPricingCard', {
      controller: Controller,
      templateUrl: 'root/landing/_components/pricing/card/card-pricing-landing.comp.html'
    });
}());
