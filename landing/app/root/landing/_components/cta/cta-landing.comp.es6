(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('landing.cta', [])
    .component('landingCta', {
      controller: Controller,
      templateUrl: 'root/landing/_components/cta/cta-landing.comp.html'
    });
}());
