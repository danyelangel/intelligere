(function () {
  'use strict';
  angular
    .module('app.modules', [
      'modules.triangles',
      'modules.article'
    ]);
}());
