(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('article.test', [])
    .component('articleTest', {
      controller: Controller,
      templateUrl: 'root/_modules/article/test/test-article.comp.html',
      bindings: {
        then: '&',
        catch: '&'
      }
    });
}());
