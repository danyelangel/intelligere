(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('modules.triangles.intro', [])
    .component('trianglesIntro', {
      controller: Controller,
      templateUrl: 'root/_modules/triangles/intro/intro-triangles.comp.html',
      bindings: {
        then: '&'
      }
    });
}());
