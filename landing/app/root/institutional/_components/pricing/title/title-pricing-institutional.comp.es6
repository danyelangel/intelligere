(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('institutional.pricing.title', [])
    .component('institutionalPricingTitle', {
      controller: Controller,
      templateUrl: 'root/institutional/_components/pricing/title/title-pricing-institutional.comp.html'
    });
}());
