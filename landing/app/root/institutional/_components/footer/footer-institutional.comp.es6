(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('institutional.footer', [])
    .component('institutionalFooter', {
      controller: Controller,
      templateUrl: 'root/institutional/_components/footer/footer-institutional.comp.html'
    });
}());
