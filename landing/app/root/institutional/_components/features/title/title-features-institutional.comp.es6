(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('institutional.features.title', [])
    .component('institutionalFeaturesTitle', {
      controller: Controller,
      templateUrl: 'root/institutional/_components/features/title/title-features-institutional.comp.html'
    });
}());
