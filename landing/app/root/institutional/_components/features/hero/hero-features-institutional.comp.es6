(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('institutional.features.hero', [])
    .component('institutionalFeaturesHero', {
      controller: Controller,
      templateUrl: 'root/institutional/_components/features/hero/hero-features-institutional.comp.html',
      bindings: {
        inverted: '<',
        title: '@',
        body: '@',
        image: '@',
        module: '@',
        raised: '@',
        isWide: '@',
        largeImage: '@'
      }
    });
}());
