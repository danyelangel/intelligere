(function () {
  'use strict';
  function reducer(action, $firedux) {
    const {credentials} = action || {},
        {email} = credentials || {};
    return $firedux.firebase.auth().sendPasswordResetEmail(email);
  }
  angular
    .module('reducers')
    .run(run);
  function run($firedux) {
    $firedux.reducer({
      trigger: 'CLIENT.AUTH.FORGOT',
      reducer: function (action) {
        return reducer(action, $firedux);
      }
    });
  }
}());
