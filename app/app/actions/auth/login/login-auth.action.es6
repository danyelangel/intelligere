(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('actions')
    .component('authLogin', {
      controller: Controller,
      templateUrl: 'actions/auth/login/login-auth.action.html',
      bindings: {
        then: '&',
        catch: '&',
        register: '&',
        forgot: '&'
      }
    });
}());
