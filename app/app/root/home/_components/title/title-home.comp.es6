(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.title', [])
    .component('homeTitle', {
      controller: Controller,
      templateUrl: 'root/home/_components/title/title-home.comp.html'
    });
}());
