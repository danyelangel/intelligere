(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level4.excersises', [])
    .component('levelsLevel4Excersises', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level4/excersises/excersises-level4-levels.comp.html'
    });
}());
