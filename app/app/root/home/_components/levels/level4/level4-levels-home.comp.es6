(function () {
  'use strict';
  class Controller {
  }
  angular
    .module('home.levels.level4', [
      'home.levels.level4.excersises'
    ])
    .component('homeLevelsLevel4', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level4/level4-levels-home.comp.html',
      bindings: {
        isActive: '<'
      }
    });
}());
