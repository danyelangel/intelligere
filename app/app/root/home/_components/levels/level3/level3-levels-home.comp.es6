(function () {
  'use strict';
  class Controller {
  }
  angular
    .module('home.levels.level3', [
      'home.levels.level3.excersises'
    ])
    .component('homeLevelsLevel3', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level3/level3-levels-home.comp.html',
      bindings: {
        isActive: '<'
      }
    });
}());
