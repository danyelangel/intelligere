(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level3.excersises', [])
    .component('levelsLevel3Excersises', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level3/excersises/excersises-level3-levels.comp.html'
    });
}());
