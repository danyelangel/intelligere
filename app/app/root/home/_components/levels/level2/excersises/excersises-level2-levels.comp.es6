(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level2.excersises', [])
    .component('levelsLevel2Excersises', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level2/excersises/excersises-level2-levels.comp.html'
    });
}());
