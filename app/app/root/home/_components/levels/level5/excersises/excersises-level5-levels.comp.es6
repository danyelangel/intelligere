(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level5.excersises', [])
    .component('levelsLevel5Excersises', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level5/excersises/excersises-level5-levels.comp.html'
    });
}());
