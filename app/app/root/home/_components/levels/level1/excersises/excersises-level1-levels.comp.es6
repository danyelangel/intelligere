(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level1.excersises', [])
    .component('levelsLevel1Excersises', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level1/excersises/excersises-level1-levels.comp.html'
    });
}());
