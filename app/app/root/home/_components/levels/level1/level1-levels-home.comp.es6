(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('home.levels.level1', [
      'home.levels.level1.excersises'
    ])
    .component('homeLevelsLevel1', {
      controller: Controller,
      templateUrl: 'root/home/_components/levels/level1/level1-levels-home.comp.html',
      bindings: {
        isActive: '<'
      }
    });
}());
