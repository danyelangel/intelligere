(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.word-pyramid.excersise', [
      'excersise.word-pyramid.excersise.component'
    ])
    .component('excersiseWordPyramidExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/word-pyramid/excersise/excersise-word-pyramid-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.word-pyramid.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-word-pyramid-excersise/>'
        });
    });
}());
