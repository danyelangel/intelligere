(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value}`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value
          };
        });
    }
  }
  angular
    .module('excersise.findNumbers.splash', [])
    .component('excersiseFindNumbersSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/findNumbers/splash/splash-findNumbers-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.findNumbers.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-find-numbers-splash/>'
        });
    });
}());
