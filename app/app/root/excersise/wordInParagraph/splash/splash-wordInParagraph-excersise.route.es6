(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            type: 'linear',
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value / 1} s`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value / 100
          };
        });
    }
  }
  angular
    .module('excersise.wordInParagraph.splash', [])
    .component('excersiseWordInParagraphSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/wordInParagraph/splash/splash-wordInParagraph-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.wordInParagraph.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-word-in-paragraph-splash/>'
        });
    });
}());
