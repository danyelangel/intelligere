(function () {
  'use strict';
  angular
    .module('excersise.wordInParagraph', [
      'excersise.wordInParagraph.splash',
      'excersise.wordInParagraph.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/wordInParagraph', '/excersise/wordInParagraph/splash');
      $stateProvider
        .state('excersise.wordInParagraph', {
          abstract: false,
          url: '/wordInParagraph',
          template: '<ui-view/>'
        });
    });
}());
