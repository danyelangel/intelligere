(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.diamondColumn.excersise', [
      'excersise.diamondColumn.excersise.component'
    ])
    .component('excersiseDiamondColumnExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/diamondColumn/excersise/excersise-diamondColumn-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.diamondColumn.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-diamond-column-excersise/>'
        });
    });
}());
