(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value}`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0.5
          }
        }
      };
    }
    parseData(data, isPercentage) {
      const _ = this._;
      return _(data.reverse())
        .map((item, index) => {
          return {
            x: index + 1 || parseInt(item.key, 10),
            y: item.value * (isPercentage ? 100 : 1)
          };
        });
    }
  }
  angular
    .module('excersise.diamondColumn.splash', [])
    .component('excersiseDiamondColumnSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/diamondColumn/splash/splash-diamondColumn-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.diamondColumn.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-diamond-column-splash/>'
        });
    });
}());
