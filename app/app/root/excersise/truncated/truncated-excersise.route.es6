(function () {
  'use strict';
  angular
    .module('excersise.truncated', [
      'excersise.truncated.splash',
      'excersise.truncated.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/truncated', '/excersise/truncated/splash');
      $stateProvider
        .state('excersise.truncated', {
          abstract: false,
          url: '/truncated',
          template: '<ui-view/>'
        });
    });
}());
