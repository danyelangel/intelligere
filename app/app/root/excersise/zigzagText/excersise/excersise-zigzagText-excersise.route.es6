(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.zigzagText.excersise', [
      'excersise.zigzagText.excersise.component'
    ])
    .component('excersiseZigzagTextExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/zigzagText/excersise/excersise-zigzagText-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.zigzagText.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-zigzag-text-excersise/>'
        });
    });
}());
