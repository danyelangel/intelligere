(function () {
  'use strict';
  angular
    .module('excersise.zigzagText', [
      'excersise.zigzagText.splash',
      'excersise.zigzagText.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/zigzagText', '/excersise/zigzagText/splash');
      $stateProvider
        .state('excersise.zigzagText', {
          abstract: false,
          url: '/zigzagText',
          template: '<ui-view/>'
        });
    });
}());
