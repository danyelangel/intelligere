(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.mergingColumns.excersise', [
      'excersise.mergingColumns.excersise.component'
    ])
    .component('excersiseMergingColumnsExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/mergingColumns/excersise/excersise-mergingColumns-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.mergingColumns.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-merging-columns-excersise/>'
        });
    });
}());
