(function () {
  'use strict';
  angular
    .module('excersise.singleColumn', [
      'excersise.singleColumn.splash',
      'excersise.singleColumn.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/singleColumn', '/excersise/singleColumn/splash');
      $stateProvider
        .state('excersise.singleColumn', {
          abstract: false,
          url: '/singleColumn',
          template: '<ui-view/>'
        });
    });
}());
