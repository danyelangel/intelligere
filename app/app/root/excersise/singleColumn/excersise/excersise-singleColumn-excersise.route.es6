(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.singleColumn.excersise', [
      'excersise.singleColumn.excersise.component'
    ])
    .component('excersiseSingleColumnExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/singleColumn/excersise/excersise-singleColumn-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.singleColumn.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-single-column-excersise/>'
        });
    });
}());
