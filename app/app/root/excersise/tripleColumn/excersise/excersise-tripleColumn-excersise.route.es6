(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.tripleColumn.excersise', [
      'excersise.tripleColumn.excersise.component'
    ])
    .component('excersiseTripleColumnExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/tripleColumn/excersise/excersise-tripleColumn-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.tripleColumn.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-triple-column-excersise/>'
        });
    });
}());
