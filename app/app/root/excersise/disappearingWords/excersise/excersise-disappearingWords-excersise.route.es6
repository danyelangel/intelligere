(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.disappearingWords.excersise', [
      'excersise.disappearingWords.excersise.component'
    ])
    .component('excersiseDisappearingWordsExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/disappearingWords/excersise/excersise-disappearingWords-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.disappearingWords.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-disappearing-words-excersise/>'
        });
    });
}());
