(function () {
  'use strict';
  angular
    .module('excersise.disappearingWords', [
      'excersise.disappearingWords.splash',
      'excersise.disappearingWords.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/disappearingWords', '/excersise/disappearingWords/splash');
      $stateProvider
        .state('excersise.disappearingWords', {
          abstract: false,
          url: '/disappearingWords',
          template: '<ui-view/>'
        });
    });
}());
