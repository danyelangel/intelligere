(function () {
  'use strict';
  class Controller {
    constructor() {
    }
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.speed.excersise', [
      'excersise.speed.excersise.component'
    ])
    .component('excersiseSpeedExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/speed/excersise/excersise-speed-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.speed.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-speed-excersise/>'
        });
    });
}());
