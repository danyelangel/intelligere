(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.wordDiamond.excersise', [
      'excersise.wordDiamond.excersise.component'
    ])
    .component('excersiseWordDiamondExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/wordDiamond/excersise/excersise-wordDiamond-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.wordDiamond.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-word-diamond-excersise/>'
        });
    });
}());
