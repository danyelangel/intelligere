(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.absentWords.excersise', [
      'excersise.absentWords.excersise.component'
    ])
    .component('excersiseAbsentWordsExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/absentWords/excersise/excersise-absentWords-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.absentWords.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-absent-words-excersise/>'
        });
    });
}());
