(function () {
  'use strict';
  angular
    .module('excersise.absentWords', [
      'excersise.absentWords.splash',
      'excersise.absentWords.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/absentWords', '/excersise/absentWords/splash');
      $stateProvider
        .state('excersise.absentWords', {
          abstract: false,
          url: '/absentWords',
          template: '<ui-view/>'
        });
    });
}());
