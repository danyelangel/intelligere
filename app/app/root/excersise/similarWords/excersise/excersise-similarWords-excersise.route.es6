(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.similarWords.excersise', [
      'excersise.similarWords.excersise.component'
    ])
    .component('excersiseSimilarWordsExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/similarWords/excersise/excersise-similarWords-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.similarWords.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-similar-words-excersise/>'
        });
    });
}());
