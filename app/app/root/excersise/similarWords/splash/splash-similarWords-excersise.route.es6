(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value} s`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value / 100
          };
        });
    }
  }
  angular
    .module('excersise.similarWords.splash', [])
    .component('excersiseSimilarWordsSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/similarWords/splash/splash-similarWords-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.similarWords.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-similar-words-splash/>'
        });
    });
}());
