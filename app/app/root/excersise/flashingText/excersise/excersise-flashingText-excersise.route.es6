(function () {
  'use strict';
  class Controller {
    constructor() {
    }
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.flashingText.excersise', [
      'excersise.flashingText.excersise.component'
    ])
    .component('excersiseFlashingTextExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/flashingText/excersise/excersise-flashingText-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.flashingText.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-flashing-text-excersise/>'
        });
    });
}());
