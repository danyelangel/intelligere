(function () {
  'use strict';
  angular
    .module('excersise.numbersWarmup4', [
      'excersise.numbersWarmup4.excersise.component'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/numbersWarmup4', '/excersise/numbersWarmup4/splash');
      $stateProvider
        .state('excersise.numbersWarmup4', {
          abstract: false,
          url: '/numbersWarmup4',
          template: '<ui-view/>'
        });
    });
}());
