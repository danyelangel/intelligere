(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.warmup.splash', [])
    .component('excersiseWarmupSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/warmup/splash/splash-warmup-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.warmup.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-warmup-splash/>'
        });
    });
}());
