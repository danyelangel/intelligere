(function () {
  'use strict';
  angular
    .module('excersise.warmup', [
      'excersise.warmup.splash',
      'excersise.warmup.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/warmup', '/excersise/warmup/splash');
      $stateProvider
        .state('excersise.warmup', {
          abstract: false,
          url: '/warmup?level',
          template: '<ui-view/>'
        });
    });
}());
