(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.warmup.excersise', [
      'excersise.warmup.excersise.component'
    ])
    .component('excersiseWarmupExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/warmup/excersise/excersise-warmup-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.warmup.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-warmup-excersise/>'
        });
    });
}());
