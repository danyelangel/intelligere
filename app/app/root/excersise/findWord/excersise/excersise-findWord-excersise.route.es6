(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.findWord.excersise', [
      'excersise.findWord.excersise.component'
    ])
    .component('excersiseFindWordExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/findWord/excersise/excersise-findWord-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.findWord.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-find-word-excersise/>'
        });
    });
}());
