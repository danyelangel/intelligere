(function () {
  'use strict';
  angular
    .module('excersise.findWord', [
      'excersise.findWord.splash',
      'excersise.findWord.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/findWord', '/excersise/findWord/splash');
      $stateProvider
        .state('excersise.findWord', {
          abstract: false,
          url: '/findWord',
          template: '<ui-view/>'
        });
    });
}());
