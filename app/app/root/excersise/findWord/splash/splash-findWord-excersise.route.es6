(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            ticks: {
              callback(value) {
                return `${value}`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value
          };
        });
    }
  }
  angular
    .module('excersise.findWord.splash', [])
    .component('excersiseFindWordSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/findWord/splash/splash-findWord-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.findWord.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-find-word-splash/>'
        });
    });
}());
