(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.doubleFixation.excersise', [
      'excersise.doubleFixation.excersise.component'
    ])
    .component('excersiseDoubleFixationExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/doubleFixation/excersise/excersise-doubleFixation-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.doubleFixation.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-double-fixation-excersise/>'
        });
    });
}());
