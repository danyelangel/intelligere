(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.recognize.excersise', [
      'excersise.recognize.excersise.component'
    ])
    .component('excersiseRecognizeExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/recognize/excersise/excersise-recognize-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.recognize.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-recognize-excersise/>'
        });
    });
}());
