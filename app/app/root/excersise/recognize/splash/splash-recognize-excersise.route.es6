(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value}%`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value * 100
          };
        });
    }
  }
  angular
    .module('excersise.recognize.splash', [])
    .component('excersiseRecognizeSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/recognize/splash/splash-recognize-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.recognize.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-recognize-splash/>'
        });
    });
}());
