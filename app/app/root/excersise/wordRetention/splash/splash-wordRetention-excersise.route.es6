(function () {
  'use strict';
  class Controller {
    constructor($window) {
      this._ = $window._;
      this.chartOptions = {
        scales: {
          yAxes: [{
            beginAtZero: false,
            ticks: {
              callback(value) {
                return `${value}%`;
              }
            }
          }],
          xAxes: [{
            type: 'time',
            ticks: {
              callback() {
                return '';
              }
            }
          }]
        },
        elements: {
          line: {
            tension: 0
          }
        }
      };
    }
    parseData(data) {
      const _ = this._;
      return _(data)
        .map((item) => {
          return {
            x: parseInt(item.key, 10),
            y: item.value * 100
          };
        });
    }
  }
  angular
    .module('excersise.wordRetention.splash', [])
    .component('excersiseWordRetentionSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/wordRetention/splash/splash-wordRetention-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.wordRetention.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-word-retention-splash/>'
        });
    });
}());
