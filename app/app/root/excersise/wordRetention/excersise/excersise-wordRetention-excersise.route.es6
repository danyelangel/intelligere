(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.wordRetention.excersise', [
      'excersise.wordRetention.excersise.component'
    ])
    .component('excersiseWordRetentionExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/wordRetention/excersise/excersise-wordRetention-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.wordRetention.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-word-retention-excersise/>'
        });
    });
}());
