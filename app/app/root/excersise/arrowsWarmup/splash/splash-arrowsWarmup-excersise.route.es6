(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('excersise.arrowsWarmup.splash', [])
    .component('excersiseArrowsWarmupSplash', {
      controller: Controller,
      templateUrl: 'root/excersise/arrowsWarmup/splash/splash-arrowsWarmup-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.arrowsWarmup.splash', {
          abstract: false,
          url: '/splash',
          template: '<excersise-arrows-warmup-splash/>'
        });
    });
}());
