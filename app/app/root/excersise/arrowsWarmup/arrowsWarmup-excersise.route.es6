(function () {
  'use strict';
  angular
    .module('excersise.arrowsWarmup', [
      'excersise.arrowsWarmup.splash',
      'excersise.arrowsWarmup.excersise'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/arrowsWarmup', '/excersise/arrowsWarmup/splash');
      $stateProvider
        .state('excersise.arrowsWarmup', {
          abstract: false,
          url: '/arrowsWarmup',
          template: '<ui-view/>'
        });
    });
}());
