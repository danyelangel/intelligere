(function () {
  'use strict';
  class Controller {
    constructor() {}
    $rand(max) {
      return Math.floor(Math.random() * max);
    }
  }
  angular
    .module('excersise.truncatedText.excersise', [
      'excersise.truncatedText.excersise.component'
    ])
    .component('excersiseTruncatedTextExcersise', {
      controller: Controller,
      templateUrl: 'root/excersise/truncatedText/excersise/excersise-truncatedText-excersise.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('excersise.truncatedText.excersise', {
          abstract: false,
          url: '/excersise',
          template: '<excersise-truncated-text-excersise/>'
        });
    });
}());
