(function () {
  'use strict';
  angular
    .module('excersise.numbersWarmup3', [
      'excersise.numbersWarmup3.excersise.component'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('/excersise/numbersWarmup3', '/excersise/numbersWarmup3/splash');
      $stateProvider
        .state('excersise.numbersWarmup3', {
          abstract: false,
          url: '/numbersWarmup3',
          template: '<ui-view/>'
        });
    });
}());
