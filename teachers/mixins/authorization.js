import { getUserObservable } from '@/helpers/auth'
import { getRef, getObservableFromRef } from '@/helpers/database/firebase'
import { switchMap, map, tap } from 'rxjs/operators'
import { Observable } from 'rxjs'

export default {
  subscriptions () {
    return {
      isAuthorized: getUserObservable().pipe(
        switchMap(user => user.institutionId
          ? getObservableFromRef(getRef(`institutions/${user.institutionId}`)).pipe(
            map(institution => [user, institution])
          )
          : Observable.from([[user]])),
        map(([user, institution]) => {
          if (!user) {
            this.$router.push('login')
          } else if (!institution) {
            this.$router.push('registration')
          } else if (!institution.license) {
            this.$router.push('registration')
          } else {
            this.$router.push('reports')
          }
        })
      )
    }
  }
}
