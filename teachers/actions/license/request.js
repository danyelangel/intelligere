export default function (rxfb) {
  return async function ({amount, method, cardToken, payer}) {
    const userId = (rxfb.auth.currentUser || {}).uid
    const licenseRequestId = rxfb.database.ref().push().key
    const institutionId = await rxfb
      .database
      .ref('users')
      .child(userId)
      .child('institutionId')
      .getValue()
      .onValue()
      .take(1)
      .toPromise()
    await rxfb
      .database
      .ref('licenseRequests')
      .child(licenseRequestId)
      .set({
        timestamp: {
          '.sv': 'timestamp'
        },
        userId,
        amount,
        method,
        cardToken: method === 'CREDIT' ? cardToken : null,
        payer
      })
    return rxfb
      .database
      .ref('institutions')
      .child(institutionId)
      .child('licenseRequestId')
      .set(licenseRequestId)
  }
}
