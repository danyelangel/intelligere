import request from './request'
export default function (rxfb) {
  return {
    request: request(rxfb)
  }
}
