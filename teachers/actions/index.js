import auth from './auth'
import guests from './guests'
import groups from './groups'
import license from './license'

export default function (rxfb) {
  return {
    auth: auth(rxfb),
    guests: guests(rxfb),
    groups: groups(rxfb),
    license: license(rxfb)
  }
}
