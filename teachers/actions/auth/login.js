export default function (rxfb) {
  return async function ({email, password}) {
    return rxfb
      .auth
      .signInWithEmailAndPassword(email, password)
  }
}
