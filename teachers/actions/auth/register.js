export default function (rxfb) {
  return async function ({email, password, adminName, displayName, address, city, phoneNumber}) {
    const auth = await rxfb
      .auth
      .createUserWithEmailAndPassword(email, password)
    const institutionId = rxfb.database.ref().push().key
    return rxfb
      .database
      .ref('institutions')
      .child(institutionId)
      .set({
        adminId: auth.uid,
        timestamp: {
          '.sv': 'timestamp'
        },
        metadata: {
          displayName,
          address,
          city,
          country: 'CO',
          phoneNumber
        }
      })
      .then(() => rxfb.database.ref('users').child(auth.uid).set({
        displayName,
        email,
        institutionId
      }))
      .catch((err) => {
        // throw new Error(err)
        return auth.delete().then(() => {
          throw new Error(err)
        })
      })
  }
}
