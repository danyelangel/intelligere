export default function (rxfb) {
  return async function () {
    return rxfb
      .auth
      .signOut()
  }
}
