import login from './login'
import logout from './logout'
import register from './register'

export default function (rxfb) {
  return {
    login: login(rxfb),
    logout: logout(rxfb),
    register: register(rxfb)
  }
}
