import _ from 'underscore'
export default function (rxfb) {
  function getUID () {
    return rxfb.database.ref('guestLists').push().key
  }
  function parseArray (guests) {
    let object = {}
    _(guests).each(({$key, displayName, dni}) => {
      object[$key || getUID()] = displayName ? {
        displayName,
        dni
      } : null
    })
    return object
  }
  function updateGuest ({eventId, uid, guestId, guest}) {
    const {displayName, dni = ''} = guest || {}
    const ref = rxfb
      .database
      .ref('guestLists')
      .child(eventId)
      .child(uid)
      .child(guestId)
    return displayName
      ? ref.update({
        displayName,
        dni
      })
      : ref.set(null)
  }
  async function updateGuests ({eventId, guests}) {
    const {uid} = await rxfb.sources.user.getAuth().take(1).toPromise()
    return Promise.all(_(guests)
      .map((guest, guestId) => {
        return updateGuest({
          eventId,
          uid,
          guestId,
          guest
        })
      }))
  }
  return async function ({guestsArray, eventId}) {
    return await updateGuests({
      guests: parseArray(guestsArray),
      eventId
    })
  }
}
