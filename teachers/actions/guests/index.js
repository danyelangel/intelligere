import save from './save'
import checkIn from './checkIn'
export default function (rxfb) {
  return {
    save: save(rxfb),
    checkIn: checkIn(rxfb)
  }
}
