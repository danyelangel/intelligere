export default function (rxfb) {
  return async function ({guestId, eventId, promotorId}) {
    return await rxfb
      .database
      .ref('guestLists')
      .child(eventId)
      .child(promotorId)
      .child(guestId)
      .child('isCheckedIn')
      .set({
        '.sv': 'timestamp'
      })
  }
}
