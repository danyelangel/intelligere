export default function (rxfb) {
  return async function ({displayName, email, password, groupId}) {
    function getInstitutionId () {
      return rxfb.sources.user.getInstitutionId().take(1).toPromise()
    }
    return await rxfb
      .database
      .ref('institutions')
      .child(await getInstitutionId())
      .child('groups')
      .child(groupId)
      .child('members')
      .push({
        displayName,
        email,
        password
      })
  }
}
