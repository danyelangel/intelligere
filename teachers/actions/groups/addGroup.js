export default function (rxfb) {
  return async function ({displayName, notes}) {
    function getInstitutionId () {
      return rxfb.sources.user.getInstitutionId().take(1).toPromise()
    }
    return await rxfb
      .database
      .ref('institutions')
      .child(await getInstitutionId())
      .child('groups')
      .push({
        displayName,
        notes,
        timestamp: {
          '.sv': 'timestamp'
        }
      })
  }
}
