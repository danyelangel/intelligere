import addGroup from './addGroup'
import addStudent from './addStudent'
export default function (rxfb) {
  return {
    addGroup: addGroup(rxfb),
    addStudent: addStudent(rxfb)
  }
}
