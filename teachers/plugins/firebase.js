import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/auth'

var config = {
  apiKey: 'AIzaSyDyu31kKT-KQ-EjDflczcf03AxP_-g3gq8',
  authDomain: 'intelligere-94e9b.firebaseapp.com',
  databaseURL: 'https://intelligere-94e9b.firebaseio.com',
  projectId: 'intelligere-94e9b',
  storageBucket: 'intelligere-94e9b.appspot.com',
  messagingSenderId: '124400065367'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
  firebase.firestore().settings({timestampsInSnapshots: true})
}

export default firebase.apps[0]
