import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  theme: {
    primary: '#A30000',
    secondary: '#004777',
    accent: '#EFD28D',
    error: '#ea4153'
  }
})
