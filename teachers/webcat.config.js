export default {
  projectId: 'webcat',
  frontendId: 'dashboard',
  database: {
    tables: {
      newsletter: {
        model: {
          displayName: {
            type: 'string',
            validations: {
              required: true
            }
          },
          email: {
            type: 'string',
            validations: {
              email: true,
              required: true
            }
          }
        },
        permissions: {
          write: true,
          create: 'auth.isAdmin || auth.vendorIds[$id]',
          update: true,
          delete: true,
          read: true,
          get: true,
          list: true
        }
      }
    }
  },
  frontend: {
    routes: [
      {
        url: '/home/:plop',
        meta: {
          title: 'Plop {{prop1}}'
        },
        component: 'root',
        props: {
          prop1: 'yeysico'
        }
      }
    ],
    components: {
      root: {
        dom: {
          el: 'div',
          attrs: {
            layout: 'column'
          },
          style: {
            background: '#F7F7F7'
          },
          class: {
            padding: true,
            margin: true
          },
          breakpoints: {
            860: {
              attrs: {},
              style: {},
              class: {}
            }
          },
          children: [
            {
              el: 'v-btn',
              template: '{{computed3}}',
              props: {
                round: true,
                outline: true
              },
              events: {
                click: {
                  method: 'callHttp'
                }
              }
            },
            {
              el: 'div',
              class: {
                'md-display-1': true,
                margin: true
              },
              template: '{{$params.plop}}'
            },
            {
              el: 'div',
              repeat: 'collection in _.sortBy(collection1, "displayName")',
              if: 'collection.num > 2',
              class: {
                'elevation-5': true,
                margin: true
              },
              children: [
                {
                  el: 'div',
                  class: {
                    padding: true
                  },
                  children: [
                    {
                      el: 'div',
                      template: 'This is {{collection.displayName}}',
                      class: {
                        right: true
                      },
                      style: {
                        color: '{{color1}}'
                      }
                    },
                    {
                      el: 'img',
                      attrs: {
                        src: '{{collection.photoURL}}'
                      },
                      style: {
                        maxWidth: '{{200 + 150}}px'
                      },
                      class: {
                        margin: true
                      }
                    },
                    {
                      el: 'script',
                      template: 'alert("This is a security vulnerability")'
                    },
                    {
                      el: 'form',
                      model: 'newsletterModel',
                      children: [
                        {
                          el: 'div',
                          children: [
                            {
                              el: 'v-text-field',
                              model: 'newsletterModel',
                              modelProp: 'displayName',
                              props: {
                                label: 'Nombre completo'
                              }
                            },
                            {
                              el: 'v-text-field',
                              model: 'newsletterModel',
                              modelProp: 'email',
                              props: {
                                label: 'Email'
                              }
                            }
                          ]
                        },
                        {
                          el: 'v-btn',
                          formId: 'newsletterForm',
                          template: 'Submit',
                          _props: {
                            loading: '$loading'
                          },
                          events: {
                            click: {
                              method: 'saveNewsletter'
                            }
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        scope: {
          string1: {
            type: 'string',
            default: 'Texto de prueba'
          },
          color1: {
            type: 'string',
            default: 'blue'
          },
          number1: {
            type: 'number',
            default: 5
          },
          computed1: {
            type: 'computed',
            template: '{{string1}} + plop'
          },
          computed2: {
            type: 'computed',
            expression: '_.map(collection1, "displayName")'
          },
          computed3: {
            type: 'computed',
            template: '{{rest1}} {{computed2}} {{$params.plop}}'
          },
          computed4: {
            type: 'computed',
            expression: '[collection1, collection1]'
          },
          collection1: {
            type: 'collection',
            tableId: 'collection1',
            order: [
              {
                prop: 'displayName',
                order: 'desc'
              }
            ]
          },
          rest1: {
            type: 'https',
            url: 'https://api.ipify.org?format=json',
            method: 'GET',
            transform: '$data.ip'
          },
          prop1: {
            type: 'prop'
          },
          newsletterModel: {
            type: 'model',
            tableId: 'newsletter',
            id: '5qRxkYKvDHAeXf9JK2iK'
          },
          registration: {
            type: 'auth',
            method: 'registration-email',
            fields: ['displayName', 'phoneNumber', 'email', 'password', 'repeatPassword', 'agreeWithTerms']
          }
        },
        methods: {
          callHttp: {
            type: 'https',
            url: 'https://google.com?plop={{string1}}',
            method: 'GET',
            data: {
              prop1: 'computed1'
            }
          },
          saveNewsletter: {
            type: 'database',
            method: 'save',
            model: 'newsletterModel'
          },
          register: {
            type: 'auth',
            method: 'registerWithEmailAndPassword'
          },
          registrationFlow: {
            type: 'pipe',
            methods: [
              {
                method: 'register',
                model: 'registration',
                transform: 'user.uid'
              },
              {
                method: 'callHttp',
                data: {
                  uid: 'uid'
                }
              }
            ]
          }
        }
      }
    }
  }
}
