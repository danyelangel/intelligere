import createGroups from './createGroups'
import createGroup from './createGroup'
import addStudent from './addStudent'
import addStudents from './addStudents'
import removeStudent from './removeStudent'
import remove from './remove'
export default {
  namespaced: true,
  actions: {
    createGroups,
    createGroup,
    addStudent,
    addStudents,
    removeStudent,
    remove
  }
}
