import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {institutionId, groupId, studentId}) {
  return getRef(`users/${studentId}/institutionId`)
    .set(null)
    .then(() => getRef(`institutions/${institutionId}/groups/${groupId}/members/${studentId}`)
      .remove())
}
