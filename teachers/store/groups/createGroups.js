import _ from 'lodash'
import createGroup from './createGroup'
export default function (ctx, {groups, institutionId}) {
  return Promise.all(_.map(groups, group => createGroup(ctx, {institutionId, group})))
}
