import axios from 'axios'
import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {institutionId, group: {displayName, user: {displayName: userName, uid, email, password}}}) {
  if (!uid) {
    return axios.post('https://us-central1-intelligere-94e9b.cloudfunctions.net/accountRequest', {
      displayName: userName,
      email,
      password,
      goToGroups: true
    }).then(({data}) => data)
      .then(uid => {
        return createGroup({institutionId, displayName, uid, userName, email})
      })
  } else {
    return createGroup({institutionId, displayName, uid, userName, email})
  }
}
function createGroup ({institutionId, displayName, uid, userName, email}) {
  return getRef(`institutions/${institutionId}/groups`).push({
    displayName,
    adminId: uid,
    adminName: userName,
    adminEmail: email,
    timestamp: {
      '.sv': 'timestamp'
    }
  })
}
