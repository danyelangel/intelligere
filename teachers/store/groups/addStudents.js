import _ from 'lodash'
import addStudent from './addStudent'
export default function (ctx, {students, groupId, institutionId}) {
  return Promise.all(_.map(students, student => addStudent(ctx, {institutionId, groupId, student})))
}
