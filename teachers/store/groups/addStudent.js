import axios from 'axios'
import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {institutionId, groupId, student: {displayName, uid, email, password}}) {
  if (!uid) {
    return axios.post('https://us-central1-intelligere-94e9b.cloudfunctions.net/accountRequest', {
      displayName,
      email,
      password
    }).then(({data}) => data)
      .then(uid => {
        return addStudent({institutionId, groupId, displayName, uid, email})
      })
  } else {
    return addStudent({institutionId, groupId, displayName, uid, email})
  }
}
function addStudent ({institutionId, groupId, displayName, uid, email}) {
  return getRef(`users/${uid}/institutionId`).set(institutionId).then(() => getRef(`institutions/${institutionId}/groups/${groupId}/members/${uid}`).set({
    displayName,
    email
  }))
}
