import _ from 'lodash'
import removeStudent from './removeStudent'
import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {institutionId, groupId}) {
  const groupRef = getRef(`institutions/${institutionId}/groups/${groupId}`)
  return groupRef
    .once('value')
    .then(snap => snap.val())
    .then(group => Promise.all(_.map(group.members, (member, studentId) => removeStudent(ctx, {institutionId, groupId, studentId}))))
    .then(groupRef.remove())
}
