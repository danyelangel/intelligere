export default {
  namespaced: true,
  state: {
    selectedGroups: [],
    timeInterval: 'monthly',
    timeIntervals: [
      {
        text: 'Diario',
        value: 'daily'
      },
      {
        text: 'Semanal',
        value: 'weekly'
      },
      {
        text: 'Mensual',
        value: 'monthly'
      },
      {
        text: 'Cada 2 meses',
        value: '2months'
      },
      {
        text: 'Cada 6 meses',
        value: '6months'
      }
    ]
  },
  mutations: {
    setTimeInterval (state, interval) {
      state.timeInterval = interval
    },
    setSelectedGroups (state, selectedGroups) {
      state.selectedGroups = selectedGroups
    }
  }
}
