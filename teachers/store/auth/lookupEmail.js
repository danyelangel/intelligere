import axios from 'axios'
export default function (ctx, email) {
  return axios.get(`https://us-central1-intelligere-94e9b.cloudfunctions.net/accountSearch?email=${email}`)
    .then(({data}) => data)
    .then(data => {
      return data || 'REGISTERED'
    })
    .catch(() => null)
}
