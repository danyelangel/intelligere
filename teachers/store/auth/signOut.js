import firebase from '@/plugins/firebase'
export default function (ctx) {
  return firebase.auth().signOut()
}
