import signInWithEmailAndPassword from './signInWithEmailAndPassword'
import signOut from './signOut'
import registerAccount from './registerAccount'
import lookupEmail from './lookupEmail'
export default {
  namespaced: true,
  actions: {
    signInWithEmailAndPassword,
    signOut,
    registerAccount,
    lookupEmail
  }
}
