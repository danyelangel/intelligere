import firebase from '@/plugins/firebase'
export default function (ctx, {email, password}) {
  return firebase.auth().signInWithEmailAndPassword(email, password)
}
