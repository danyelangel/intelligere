import firebase from '@/plugins/firebase'
import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {email, password, displayName, phoneNumber, institutionName}) {
  return firebase.auth()
    .createUserWithEmailAndPassword(email, password)

    .then(({user: {uid}}) => {
      return getRef(`/users/${uid}`).set({
        displayName,
        phoneNumber,
        email
      }).then(() => ({uid}))
    })
    .then(({uid}) => {
      return getRef(`/institutions`).push({
        displayName,
        phoneNumber,
        email
      })
    })
}
