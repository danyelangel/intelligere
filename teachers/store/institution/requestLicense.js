import {getRef} from '@/helpers/database/firebase'
export default function (ctx, {amount, institutionId}) {
  return getRef(`institutions/${institutionId}/params/licenseAmount`).set(amount)
}
