import create from './create'
import requestLicense from './requestLicense'
export default {
  namespaced: true,
  actions: {
    create,
    requestLicense
  }
}
