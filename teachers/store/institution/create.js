import {getRef} from '@/helpers/database/firebase'
import firebase from '@/plugins/firebase'
export default function (ctx, {displayName, phoneNumber, address, city, country}) {
  const ref = getRef(`institutions`).push()
  const institutionId = ref.key
  const {uid} = firebase.auth().currentUser
  return ref.set({
    adminId: uid,
    timestamp: {
      '.sv': 'timestamp'
    },
    metadata: {
      displayName,
      phoneNumber,
      address,
      city,
      country
    }
  }).then(() => getRef(`users/${uid}/institutionId`).set(institutionId))
}
