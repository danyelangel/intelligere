import Vuex from 'vuex'
import auth from './auth'
import reports from './reports'
import institution from './institution'
import groups from './groups'

const createStore = () => {
  return new Vuex.Store({
    state: {
      colors: [
        '#9C27B0',
        '#2196F3',
        '#00BCD4',
        '#4CAF50',
        '#FFC107',
        '#795548'
      ],
      colorsTranslucent: [
        'rgba(156,39,176 ,0.5)',
        'rgba(33,150,243 ,0.5)',
        'rgba(0,188,212 ,0.5)',
        'rgba(76,175,80 ,0.5)',
        'rgba(255,193,7 ,0.5)',
        'rgba(121,85,72 ,0.5)'
      ],
      isSidenavOpen: false
    },
    modules: {
      auth,
      reports,
      institution,
      groups
    },
    mutations: {
      toggleSidenav (state) {
        state.isSidenavOpen = !state.isSidenavOpen
      },
      setSidenavState (state, isOpen) {
        state.isSidenavOpen = isOpen
      }
    }
  })
}

export default createStore
