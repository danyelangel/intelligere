import _ from 'lodash'
import {Model} from '@/helpers/database'
import {compileTemplate, compileExpression} from '@/helpers/compilers'
export function getInitialScope ({scope, database, projectId}) {
  return _.mapValues(scope, (item, key) => {
    return getItemDefault(item, key)
  })
  function getItemDefault (item, key) {
    const {type, default: defaultValue, tableId, id} = item
    switch (type) {
      case 'string':
        return defaultValue
      case 'number':
        return defaultValue
      case 'model':
        return getModel({projectId, tableId, id, database})
      case 'auth':
        return {}
      default:
        return null
    }
  }
  function getModel ({projectId, tableId, id, database}) {
    return new Model({projectId, tableId, database})
  }
}
export function parseProps (scope) {
  return _(scope)
    .map((item, key) => [key, item])
    .filter(([key, {type}]) => type === 'prop')
    .map(([key]) => key)
    .value()
}
export function watchScope (scope) {
  return function () {
    const newScope = digest(scope, this.scope, this) || {}
    this.$emit('scope', this.scope)
    if (!_.isEqual(newScope, this.scope)) {
      this.scope = newScope
    }
  }
}
export function digest (scope, $scope, self) {
  return _.mapValues($scope, (item, key) => {
    let returnable
    if ((scope[key] || {}).type === 'computed') {
      const {expression, template} = scope[key]
      if (expression) {
        returnable = compileExpression(expression, $scope)
      }
      if (template) {
        returnable = compileTemplate(template, $scope)
      }
    } else if ((scope[key] || {}).type === 'prop') {
      returnable = self[key]
    } else {
      returnable = item
    }
    return returnable
  })
}
