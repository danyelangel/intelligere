import {compileExpression} from '@/helpers/compilers'
export default function ({model}) {
  return function () {
    const databaseModel = compileExpression(model, this.scope)
    this.scope.$loading = true
    databaseModel.$save().then(() => {
      this.scope.$loading = false
    }).catch(() => {
      this.scope.$loading = false
      this.scope.$error = true
    })
  }
}
