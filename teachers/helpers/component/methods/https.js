import _ from 'lodash'
import axios from 'axios'
import {compileTemplate, compileExpression} from '@/helpers/compilers'
export default function ({method, url, data}) {
  return function () {
    this.scope.$loading = true
    axios({
      method,
      url: compileTemplate(url, this.scope),
      query: _.mapValues(data, scopeId => compileExpression(scopeId, this.scope)),
      data: _.mapValues(data, scopeId => compileExpression(scopeId, this.scope))
    }).then(() => {
      this.scope.$loading = false
    }).catch(() => {
      this.scope.$loading = false
      this.scope.$error = true
    })
  }
}
