import _ from 'lodash'
import getHttps from './https'
import getDatabase from './database'
export function getComponentMethods (methods) {
  const methodDefinitions = [
    ['https', getHttps],
    ['database', getDatabase]
  ]
  const [httpsMethods, databaseMethods] = _.map(methodDefinitions, ([type, fn]) => _(methods)
    .map((item, key) => [key, item])
    .filter(([, {type: itemType}]) => type === itemType)
    .map(([key, item]) => [key, fn(item)])
    .value())
  return _.fromPairs([
    ...httpsMethods,
    ...databaseMethods
  ])
}
