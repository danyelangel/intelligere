import {map} from 'rxjs/operators'
import {Model, getObservableFromDoc, getDocumentRef} from '@/helpers/database'
export default function ({projectId, database, tableId, id}) {
  const model = new Model({projectId, tableId, id, database})
  return getObservableFromDoc(
    getDocumentRef({projectId, tableId, id})
  ).pipe(
    map(data => model.$set(data))
  )
}
