import _ from 'lodash'
import {map} from 'rxjs/operators'
import {getCollectionQuery, getObservableFromCollection} from '@/helpers/database'
export default function ({projectId, database, tableId, order}) {
  return getObservableFromCollection(
    getCollectionQuery({projectId, tableId, order})
  ).pipe(
    map(items => _.map(items, data => ({
      ...data,
      $projectId: projectId,
      $tableId: tableId
    })))
  )
}
