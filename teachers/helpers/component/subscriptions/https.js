import axios from 'axios'
import {Observable} from 'rxjs'
import {compileExpression} from '@/helpers/compilers'
export default function ({url, method, transform}) {
  return Observable.create(observer => {
    axios({
      method,
      url
    })
      .then(({data}) => compileExpression(transform, {$data: data}))
      .then(data => observer.next(data))
  })
}
