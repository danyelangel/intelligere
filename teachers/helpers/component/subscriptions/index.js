import _ from 'lodash'
import {tap} from 'rxjs/operators'
import {getAuth$} from '@/helpers/auth'
import getCollectionObservable from './collection'
import getHttpsObservable from './https'
import getDocumentObservable from './document'
export function getSubscriptionsFn ({scope, projectId, database}) {
  return function () {
    const documents = _(scope)
      .map((item, key) => ([key, item]))
      .filter(([key, {type, id}]) => type === 'model' && !!id)
      .map(([key, params]) => [key, getDocumentObservable({...params, projectId, database})])
      .value()
    const collections = _(scope)
      .map((item, key) => ([key, item]))
      .filter(([key, {type}]) => type === 'collection')
      .map(([key, params]) => [key, getCollectionObservable({...params, projectId, database})])
      .value()
    const https = _(scope)
      .map((item, key) => ([key, item]))
      .filter(([key, {type}]) => type === 'https')
      .map(([key, params]) => [key, getHttpsObservable({...params, projectId, database})])
      .value()
    return _.mapValues(
      _.fromPairs([
        ...documents,
        ...collections,
        ...https,
        ['$auth', getAuth$()]
      ]),
      (observable, scopeId) => observable.pipe(
        tap(data => {
          this.scope = {
            ...this.scope,
            [scopeId]: data
          }
        })
      )
    )
  }
}
