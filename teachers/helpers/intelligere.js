import _ from 'lodash'
import {getRef, getObservableFromRef} from '@/helpers/database/firebase'
import {getUserObservable} from '@/helpers/auth'
import {NIL_OBSERVABLE} from '@/helpers/observable'
import {map, switchMap, filter, catchError} from 'rxjs/operators'
import { combineLatest, of } from 'rxjs'
// Observables
export function getGroupsObservable (getAll, instId) {
  return combineLatest([
    getUserObservable(),
    getInstitutionObservable(instId)
  ])
    .pipe(
      filter(([user, institution]) => !!user),
      filter(([{institutionId}]) => instId || institutionId),
      switchMap(([{institutionId, uid}, institution]) => getInstitutionGroupsObservable(instId || institutionId).pipe(
        map(groups => institution.adminId === uid || getAll
          ? groups
          : _.filter(groups, group => group.adminId === uid))
      ))
    )
  function getInstitutionGroupsObservable (institutionId) {
    return getObservableFromRef(
      getInstitutionGroupsRef(institutionId)
    ).pipe(
      map(groups => _.mapValues(groups, (group, groupId) => ({...group, groupId}))),
      map(_.toArray)
    )
  }
  function getInstitutionGroupsRef (institutionId) {
    return getRef()
      .child('institutions')
      .child(institutionId || '-')
      .child('groups')
  }
}
export function getStudentsObservable (instId) {
  return getGroupsObservable(true, instId)
    .pipe(
      switchMap(groups => combineLatest(
        _(groups)
          .map(group => _.map((group.members), (member, uid) => ({...member, uid})))
          .flatten()
          .map(({uid, displayName, email}) => getMemberObservable(uid).pipe(map(student => ({...student, studentId: uid, displayName, email})), catchError((err) => console.error(err) || of({ err: true }))))
          .value())
      )
    )
  function getMemberObservable (studentId) {
    return getObservableFromRef(getMemberRef(studentId))
  }
  function getMemberRef (studentId) {
    return getRef()
      .child('students')
      .child(studentId)
  }
}
export function getInstitutionObservable (instId) {
  function getInstitutionObservable (institutionId) {
    return getObservableFromRef(getRef()
      .child('institutions')
      .child(institutionId))
      .pipe(
        map(institution => ({...institution, institutionId}))
      )
  }
  return getUserObservable().pipe(
    switchMap(user => instId || (user || {}).institutionId
      ? getInstitutionObservable(instId || user.institutionId)
      : NIL_OBSERVABLE)
  )
}
export function getScoresObservable () {
  return getStudentsObservable().pipe(
    map(students => _(students)
      .filter(student => !!student)
      .map(({studentId, scores}) => _.map(scores, score => ({...score, studentId})))
      .flatten()
      .value())
  )
}
export function getSessionsObservable () {
  return getStudentsObservable().pipe(
    map(students => _(students)
      .filter(student => !!student)
      .map(({studentId, sessions}) => _.map(sessions, session => ({...session, studentId})))
      .flatten()
      .value())
  )
}
export function getStructureObservable () {
  return getObservableFromRef(getRef().child('structure'))
}

// Pure Functions
export function getAutocompleteGroups ({students, groups}) {
  return _(groups)
    .map(group => getGroupItems({group, students}))
    .flattenDeep()
    .filter(({displayName, divider, header}) => displayName || divider || header)
    .value()
  function getGroupItems ({group, students}) {
    const {displayName, groupId} = group
    const groupStudents = getStudents({group, students})
    return [
      {divider: true},
      {
        groupId,
        displayName,
        isGroup: true,
        students: groupStudents
      },
      getStudentGroups(groupStudents)
    ]
  }
  function getStudents ({group, students}) {
    const studentsObj = _.fromPairs(_.map(students, (student) => [student.studentId, student]))
    return _(group.members)
      .map((member, studentId) => studentsObj[studentId])
      .filter(data => data)
      .value()
  }
  function getStudentGroups (students) {
    return _.map(students, ({studentId, displayName}) => ({
      groupId: studentId,
      studentId,
      displayName,
      students: [
        {
          studentId,
          displayName
        }
      ]
    }))
  }
}
export function getReport ({scores, sessions, groups, students, selectedGroups, structure, start, end}) {
  const groupStudentIds = _.map(selectedGroups, uid =>
    uid.length > 20
      ? [[uid], getStudent(uid)]
      : [getStudentIds(uid), getGroup(uid)])
  const sessionsPerGroup = _.map(groupStudentIds, ([studentIds]) => _(sessions)
    .compact()
    .filter(session => session.progress)
    .filter(session => _.includes(studentIds, session.studentId))
    .filter(score => score.createdAt >= start.valueOf() && score.createdAt <= end.valueOf())
    .map(transformSession)
    .sortBy('createdAt')
    .value())
  const scoresPerGroup = _.map(groupStudentIds, ([studentIds, groupOrStudent]) => ({
    level: getCurrentLevel((groupOrStudent || {}).progress),
    scores: _(scores)
      .filter(score => _.includes(studentIds, score.studentId))
      .filter(score => score.timestamp >= start.valueOf() && score.timestamp <= end.valueOf())
      .map(score => score.wpm
        ? {
          ...score,
          ewpm: score.wpm * (score.comprehension || 1)
        }
        : score)
      .tap(console.log)
      .sortBy('timestamp')
      .value()
  }))
  console.log(scoresPerGroup, 'scoresPerGroup')
  const scoresMatrix = _.map(scoresPerGroup, ({scores, level}, groupIndex) => ({
    sessions: sessionsPerGroup[groupIndex],
    level,
    ..._.groupBy(scores, 'lessonId')
  }))
  const groupMatrix = transformMatrix(scoresMatrix, scores => {
    const duration = getTransforms(_.map(scores, 'duration'), true)
    const wpm = getTransforms(_.map(scores, 'wpm'))
    const comprehension = getTransforms(_.map(scores, 'comprehension'))
    const ewpm = getTransforms(_.map(scores, 'ewpm'))
    const percentage = getTransforms(_.map(scores, 'percentage'))
    const number = getTransforms(_.map(scores, 'number'))
    const milliseconds = getTransforms(_.map(scores, 'milliseconds'))
    const metrics = _.filter([
      ['duration', duration],
      ['wpm', wpm],
      ['comprehension', comprehension],
      ['ewpm', ewpm],
      ['percentage', percentage],
      ['number', number],
      ['milliseconds', milliseconds]
    ], ([metric, values]) => values)
    return _.fromPairs(metrics)
  })
  const measurements = _.flattenDeep(
    _.map(groupMatrix, (group, groupIndex) =>
      _.map(group, (lesson, lessonId) => _.isObject(lesson)
        ? _.map(lesson, (metric, metricId) => ({
          ...metric,
          metricId,
          lessonId,
          groupIndex
        }))
        : ({
          level: lesson,
          groupIndex,
          metricId: 'level',
          lessonId: 'level'
        })
      )
    )
  )
  const metrics = mergeTransforms(_.groupBy(measurements, 'metricId'), true)
  console.log(metrics, 'metrics')
  const lessons = _.groupBy(measurements, 'lessonId')
  const metricsByLesson = _.mapValues(lessons, lesson => _.groupBy(lesson, 'metricId'))
  console.log(metricsByLesson, 'metricsByLesson')
  return {
    ...metricsByLesson,
    metrics,
    start,
    end
  }
  function getCurrentLevel (progress) {
    const keys = _.keys(progress)
    return (_.size(keys) || 2) - 1
  }
  function transformSession (session) {
    return {
      ...session,
      progressPercentage: _.sum(session.progress),
      duration: session.finishedAt ? session.finishedAt - session.createdAt : 0
    }
  }
  function getTransforms (data, ignoreZeroes) {
    data = _(data)
      .filter(item => _.isNumber(item))
      .value()
    if (!_.size(data)) {
      return null
    } else {
      const max = _.max(ignoreZeroes ? _.compact(data) : data)
      const min = _.min(ignoreZeroes ? _.compact(data) : data)
      const mean = _.mean(ignoreZeroes ? _.compact(data) : data)
      const count = _.size(data)
      const improvement = min / max
      return {
        list: data,
        max,
        min,
        mean,
        count,
        improvement
      }
    }
  }
  function mergeTransforms (data, ignoreZeroes) {
    return _.mapValues(data, metric =>
      _.map(
        _.groupBy(metric, 'groupIndex'),
        (group, groupIndex) => ({
          ...getTransforms(
            _(group)
              .map('list')
              .flatten()
              .value(),
            ignoreZeroes
          ),
          groupIndex: parseInt(groupIndex, 10)
        })
      )
    )
  }
  function getStudentIds (groupId) {
    return _(groups)
      .filter(({groupId: currentGroupId}) => currentGroupId === groupId)
      .map(({members}) => _.keys(members))
      .head()
  }
  function transformMatrix (durationMatrix, fn) {
    return _.map(durationMatrix, group => _.mapValues(group, data => _.isArray(data) ? fn(data) : data))
  }
  function getStudent (uid) {
    return _(students)
      .filter(student => student.studentId === uid)
      .head()
  }
  function getGroup (uid) {
    return _(groups)
      .filter(group => group.groupId === uid)
      .head()
  }
}
