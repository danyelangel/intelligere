import _ from 'lodash'
export function getMatchingRoute (path, routes) {
  const routeDefinitions = _.map(routes, route => ({...route, slices: getSlices(route.url)}))
  const pathSlices = _.compact(path.split('/'))
  const {component: componentId, query, props, meta} = getPathRoute({routeDefinitions, pathSlices})
  return {
    componentId,
    query,
    props,
    meta
  }
}
function getSlices (url) {
  return _.map(_.compact(url.split('/')), (pathSlice) => ({
    type: pathSlice.indexOf(':') === 0 ? 'variable' : 'constant',
    value: pathSlice.replace(':', '')
  }))
}
function getPathRoute ({routeDefinitions, pathSlices}) {
  return _(routeDefinitions)
    .filter(({slices}) => matchesAll({slices, pathSlices}) && _.isEqual(_.size(slices), _.size(pathSlices)))
    .map((result) => ({
      ...result,
      query: getQueryParams({slices: result.slices, pathSlices})
    }))
    .head() || {}
}
function matchesAll ({slices, pathSlices}) {
  return _.every(slices, ({type, value}, index) => {
    return type === 'constant'
      ? pathSlices[index] === value
      : !!pathSlices[index]
  })
}
function getQueryParams ({slices, pathSlices}) {
  return _.fromPairs(_(slices)
    .map(({type, value}) => type === 'variable' ? value : undefined)
    .map((itemId, index) => itemId ? [itemId, decodeURI(pathSlices[index])] : null)
    .filter(data => !!data)
    .value())
}
