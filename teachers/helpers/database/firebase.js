import _ from 'lodash'
import firebase from '@/plugins/firebase'
import {Observable} from 'rxjs'
// const DB_ENV = 'production'
export function getCollectionRef ({projectId, tableId}) {
  return firebase.firestore()
    .collection('projects').doc(projectId)
    .collection('database').doc(tableId)
    .collection('records')
}
export function getDocumentRef ({projectId, tableId, id}) {
  return firebase.firestore()
    .collection('projects').doc(projectId)
    .collection('database').doc(tableId)
    .collection('records').doc(id)
}
export function getCollectionQuery ({projectId, tableId, order}) {
  let collectionRef = getCollection({projectId, tableId})
  _.each(order, ({prop, order}) => {
    collectionRef = collectionRef.orderBy(prop, order || 'asc')
  })
  return collectionRef
}
export function getObservableFromCollection (collectionRef) {
  return Observable.create(observer => {
    collectionRef.onSnapshot(snap => {
      let data = []
      snap.forEach(doc => {
        data.push({...doc.data(), $id: doc.id})
      })
      observer.next(data)
    })
  })
}
export function getObservableFromDoc (docRef) {
  return Observable.create(observer => {
    docRef.onSnapshot(snap => {
      observer.next(snap.data())
    })
  })
}
export function getObservableFromRef (ref) {
  return Observable.create(observer => {
    ref.on('value', snap => {
      observer.next(snap.val())
    }, err => {
      observer.error(err)
    })
  })
}
export function getDatabaseRef (path) {
  const ref = firebase.database().ref('/production')
  return path ? ref.child(path) : ref
}
export const getRef = getDatabaseRef
function getCollection ({projectId, tableId}) {
  return firebase.firestore()
    .collection('projects').doc(projectId)
    .collection('database').doc(tableId)
    .collection('records')
}
