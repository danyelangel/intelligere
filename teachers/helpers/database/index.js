import _ from 'lodash'
import { getCollectionRef, getDocumentRef } from './firebase'
export { getCollectionRef, getDocumentRef, getCollectionQuery, getObservableFromCollection, getObservableFromDoc } from './firebase'
export class Model {
  constructor ({projectId, tableId, id, database}) {
    this.$projectId = projectId
    this.$tableId = tableId
    this.$database = database
    this.$id = id
    this.$model = ((database.tables || {})[tableId] || {}).model
    _.each(this.$model, ({type, default: defaultValue, validations}, key) => {
      this[key] = defaultValue || getDefaultFromType(type)
    })
  }
  $set (data) {
    _.each(this.$model, ({type, default: defaultValue, validations}, key) => {
      this[key] = data[key] || defaultValue || getDefaultFromType(type)
    })
    return this
  }
  $save () {
    const {$projectId: projectId, $tableId: tableId} = this
    if (this.$id) {
      // Update existing
      return getDocumentRef({projectId, tableId, id: this.$id}).update(this.get())
    } else {
      // Create new
      return getCollectionRef({projectId, tableId}).add(this.get())
    }
  }
  get () {
    return _.mapValues(this.$model, (item, key) => this[key])
  }
}
export function getDefaultFromType (type) {
  switch (type) {
    case 'string':
      return ''
    case 'number':
      return '0'
    case 'boolean':
      return false
    default:
      return null
  }
}
