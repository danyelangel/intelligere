import firebase from '@/plugins/firebase'
import {Observable} from 'rxjs'
import {switchMap, map} from 'rxjs/operators'
import {NIL_OBSERVABLE} from '@/helpers/observable'
import {getObservableFromRef, getDatabaseRef} from '@/helpers/database/firebase'

export function getAuthObservable () {
  return Observable.create(observer => {
    firebase.auth().onAuthStateChanged(user => {
      observer.next(user)
    })
  })
}
export function getUserObservable () {
  return getAuthObservable().pipe(
    switchMap(auth => auth ? getObservableFromRef(getUserRef(auth.uid)).pipe(map(user => ({uid: auth.uid, ...user}))) : NIL_OBSERVABLE)
  )
}
function getUserRef (uid) {
  return getDatabaseRef(`users/${uid}`)
}
