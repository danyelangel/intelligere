import expr from 'expression-eval'
import _ from 'lodash'
_.templateSettings.interpolate = /{{([\s\S]+?)}}/g
export function compileTemplate (template, scope, isVerbose) {
  let returnable = ''
  let error
  try {
    returnable = _.template(template)(scope)
  } catch (err) {
    error = err
    console.warn(`Webcat runtime error: Template "${template}" could not be rendered.`)
  }
  return isVerbose
    ? {
      value: returnable,
      error
    }
    : returnable
}
export function compileExpression (expression, scope, isVerbose = false) {
  let returnable, error
  try {
    returnable = expr.eval(expr.parse(expression), {...scope, _})
  } catch (err) {
    error = err
    console.warn(`Webcat runtime error: Expression "${expression}" threw an error.`, err)
  }
  return isVerbose
    ? {
      value: returnable,
      error
    }
    : returnable
}
