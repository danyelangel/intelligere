import _ from 'lodash'
import {compileTemplate, compileExpression} from '@/helpers/compilers'
export function getElement (element, h, scope, self) {
  const {
    el,
    // Options
    attrs,
    props,
    _props,
    events,
    class: classes,
    style,
    // Content
    children,
    template,
    // Webcat
    model: modelRef,
    modelProp
  } = element
  const compileAll = (obj, scope, fn) => _.mapValues(obj, expression => (fn)(expression, scope))
  const boundAttrs = compileAll(attrs, scope, compileTemplate)
  const boundStyle = compileAll(style, scope, compileTemplate)
  const boundProps = compileAll(_props, scope, compileExpression)
  const model = modelRef ? compileExpression(modelRef, scope) : null
  const boundValue = model && modelProp ? {value: model[modelProp]} : {}
  const boundInput = model && modelProp ? {input: function (value) { model[modelProp] = value }} : {}
  const SCRIPT_ERROR = 'console.warn("Webcat compiler error: Inline scripts not allowed")'
  const compiledContent = el !== 'script' ? compileTemplate(template, scope) : SCRIPT_ERROR
  return h(
    el,
    // Options
    {
      props: {
        ...props,
        ...boundProps,
        ...boundValue
      },
      attrs: boundAttrs,
      style: boundStyle,
      class: classes,
      on: {
        ..._.mapValues(events, ({method}) => self[method]),
        ...boundInput
      }
    },
    // Content
    compiledContent || getChildren(children, scope, h, self)
  )
}
function getChildren (children, scope, h, self) {
  return _(children)
    .map(child => {
      const {if: condition = 'true', repeat} = child
      // v-for
      if (repeat) {
        const [childScopeId, collectionExpression] = repeat.trim().split(' in ')
        const collection = compileExpression(collectionExpression, scope)
        return _.map(collection, subscope => {
          const childScope = {...scope, [childScopeId]: subscope}
          const validation = compileExpression(condition, childScope)
          return validation ? getElement(child, h, childScope, self) : null
        })
      // v-if
      } else if (compileExpression(condition, scope)) {
        return [getElement(child, h, scope, self)]
      }
    })
    .compact()
    .flatten()
    .compact()
    .value()
}
