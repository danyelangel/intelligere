(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('content.find-word.update', [
      'content.find-word.update.reducer'
    ])
    .component('contentFindWordUpdate', {
      controller: Controller,
      templateUrl: 'root/content/find-word/_actions/update/update-find-word-content.action.html',
      bindings: {
        wordId: '<',
        then: '&',
        catch: '&'
      }
    });
}());
