(function () {
  'use strict';
  class Controller {
    constructor($firedux) {
      this.$firedux = $firedux;
      this.$uid = this.$firedux.var('UID');
    }
  }
  angular
    .module('content.articles.list', [])
    .component('contentArticlesList', {
      controller: Controller,
      templateUrl: 'root/content/articles/list/list-articles-content.route.html'
    })
    .config(function ($stateProvider) {
      $stateProvider
        .state('content.articles.list', {
          abstract: false,
          url: '/',
          template: '<content-articles-list/>'
        });
    });
}());
