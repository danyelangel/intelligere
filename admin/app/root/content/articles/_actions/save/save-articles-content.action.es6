(function () {
  'use strict';
  class Controller {
    constructor() {}
  }
  angular
    .module('content.articles.save', [
      'content.articles.save.reducer'
    ])
    .component('contentArticlesSave', {
      controller: Controller,
      templateUrl: 'root/content/articles/_actions/save/save-articles-content.action.html',
      bindings: {
        articleId: '<',
        article: '<',
        then: '&',
        catch: '&'
      }
    });
}());
