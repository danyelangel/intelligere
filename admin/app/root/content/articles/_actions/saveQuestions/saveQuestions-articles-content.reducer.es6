(function () {
  'use strict';
  function validate(action = {}) {
    let returnable;
    if (
      angular.isString(action.articleId) &&
      angular.isObject(action.questions)
    ) {
      returnable = true;
    }
    return returnable;
  }
  function reducer(action, $firedux) {
    return $firedux
      .ref('content/articleQuestions')
      .child(action.articleId)
      .set(action.questions);
  }
  angular
    .module('content.articles.saveQuestions.reducer', [])
    .run(run);
  function run($firedux) {
    $firedux.reducer({
      trigger: 'CONTENT.ARTICLES.SAVE_QUESTIONS',
      reducer: function (action) {
        return validate(action) ?
          reducer(action, $firedux) :
          Promise.reject('VALIDATION ERROR');
      }
    });
  }
}());
